# FW1 - Exemplo validação certificado API REST

Projeto com exemplo de validação de certificado via API REST utilizando o FW1

## Validação de certificado através de basic authentication

Para realização do exemplo da classe ExemploValidacaoBasicAuthentication.java as seguintes variáveis devem ser preenchidas:

| Variável | Descrição | Exemplo |
| ------ | ------ | ------ |
| URL_FW | URL do framework | https://fw.bry.com.br |
| CERTIFICATE_TO_VALIDATE | Caminho da localização do certificado que será validado | C:/Users/Documents/certificate.cer |
| USERNAME_FW1 | Usuário cadastrado no FW1, representado pelo CPF | 11111111111 |
| PASSWORD_FW1 | Senha definida para o usuário | senha1234 |

## Validação de certificado através de autenticação via certificado digital

Para realização do exemplo da classe ExemploValidacaoCertificadoSSL.java as seguintes variáveis devem ser preenchidas:

| Variável | Descrição | Exemplo |
| ------ | ------ | ------ |
| URL_FW | URL do framework | https://fw.bry.com.br |
| CERTIFICATE_TO_VALIDATE | Caminho da localização do certificado que será validado | C:/Users/Documents/certificate.cer |
| CAMINHO_CERTIFICADO_PARA_AUTENTICACAO | Certificado do cliente que será usado para autenticação no formato PKCS12  | C:/Users/Documents/certificate.p12 |
| SENHA_CERTIFICADO_PARA_AUTENTICACAO | Senha do certificado | senha1234 |
| CAMINHO_TRUST_STORE | Caminho do arquivo truststore em formato JKS | C:/Users/Documents/truststore.jks |
| SENHA_TRUST_STORE | Senha do truststore | senha1234 |

## Avaliação da resposta obtida

A pasta /src/main/resources possui o arquivo de referência **FW1 Validador REST v3.pdf**, que contém todos os status possíveis do retorno de uma validação