package br.com.bry.framework.exemplo.validacao_certificado;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.FileUtils;

public class ExemploValidacaoBasicAuthentication {

	private static final String CERTIFICATE_TO_VALIDATE = System.getProperty("user.dir") + "/src/main/resources/exemplo_certificado_bry_multipla_v4.cer";

	private static final String URL_FW = "https://fw.bry.com.br";

	private static final String USERNAME_FW1 = "usuario(cpf)";

	private static final String PASSWORD_FW1 = "senha";

	public static void main(String[] args) {

		try {
			File file = new File(CERTIFICATE_TO_VALIDATE);
			URL url = new URL(URL_FW + "/validador/rs/verificarCertificado");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/x-x509-ca-cert");
			con.setDoOutput(true);

			// basic auth
			String auth = USERNAME_FW1 + ":" + PASSWORD_FW1;
			byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
			String authHeaderValue = "Basic " + new String(encodedAuth);
			con.setRequestProperty("Authorization", authHeaderValue);

			DataOutputStream request = new DataOutputStream(con.getOutputStream());
			request.write(FileUtils.readFileToByteArray(file));
			request.flush();

			if (con.getResponseCode() == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String strCurrentLine;
				while ((strCurrentLine = br.readLine()) != null) {
					System.out.println(strCurrentLine);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
