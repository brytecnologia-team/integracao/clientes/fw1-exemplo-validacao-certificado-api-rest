package br.com.bry.framework.exemplo.validacao_certificado;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.io.FileUtils;

public class ExemploValidacaoCertificadoSSL {

	private static final String URL_FW = "https://fw.bry.com.br";
	private static final String CERTIFICATE_TO_VALIDATE = System.getProperty("user.dir") + "/src/main/resources/exemplo_certificado_bry_multipla_v4.cer";

	private static final String CAMINHO_CERTIFICADO_PARA_AUTENTICACAO = "C:/Users/Documents/certificate.p12";
	private static final String SENHA_CERTIFICADO_PARA_AUTENTICACAO = "senha1234";

	private static final String CAMINHO_TRUST_STORE = System.getProperty("user.dir") + "/src/main/resources/repositorio_ssl_confiavel_completo.jks";
	private static final String SENHA_TRUST_STORE = "123bry";

	public static void main(String[] args) {

		try {

			// Cria o keystore do cliente que será utilizada para a autenticação no
			// servidor.
			KeyManager[] keyManagers = createKeyManager();

			// Cria o trustore com ACs predefinidas para confiar no certificado SSL
			// retornado pelo servidor
			TrustManager[] trustManagers = createTrustManager();

			// Criação do contexto SSL a partir do keystore e truststore configurados
			SSLSocketFactory socketFactory = createSSLSocketFactory(keyManagers, trustManagers);

			// Definição dos parâmetros necessários para utilização da API Rest
			URL url = new URL(URL_FW + "/validador/rs/verificarCertificado");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setSSLSocketFactory(socketFactory);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/x-x509-ca-cert");
			con.setDoOutput(true);
			con.setDoInput(true);
			File file = new File(CERTIFICATE_TO_VALIDATE);
			DataOutputStream request = new DataOutputStream(con.getOutputStream());
			request.write(FileUtils.readFileToByteArray(file));
			request.flush();

			// Imprime resposta da validação
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String strCurrentLine;
			if (con.getResponseCode() == 200) {
				while ((strCurrentLine = br.readLine()) != null) {
					System.out.println(strCurrentLine);
				}
			}
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static SSLSocketFactory createSSLSocketFactory(KeyManager[] keyManagers, TrustManager[] trustManagers) throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(keyManagers, trustManagers, new java.security.SecureRandom());
		SSLContext.setDefault(sslContext);
		SSLSocketFactory socketFactory = sslContext.getSocketFactory();
		return socketFactory;
	}

	private static KeyManager[] createKeyManager() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, UnrecoverableKeyException {
		// keyManagers
		KeyStore clientStore = KeyStore.getInstance("PKCS12");
		clientStore.load(new FileInputStream(CAMINHO_CERTIFICADO_PARA_AUTENTICACAO), SENHA_CERTIFICADO_PARA_AUTENTICACAO.toCharArray());

		KeyManager[] keyManagers;
		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance("SunX509");
		keyFactory.init(clientStore, SENHA_CERTIFICADO_PARA_AUTENTICACAO.toCharArray());
		keyManagers = keyFactory.getKeyManagers();
		return keyManagers;
	}

	private static TrustManager[] createTrustManager() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, FileNotFoundException {
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(new FileInputStream(CAMINHO_TRUST_STORE), SENHA_TRUST_STORE.toCharArray());

		TrustManager[] trustManagers;
		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance("SunX509");
		trustFactory.init(trustStore);
		trustManagers = trustFactory.getTrustManagers();
		return trustManagers;
	}

}
